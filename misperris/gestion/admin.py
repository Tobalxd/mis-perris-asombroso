from django.contrib import admin
from .models import Estado, Perro

# Register your models here.

admin.site.register(Estado)
admin.site.register(Perro)

