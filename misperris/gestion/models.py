from django.db import models

# Create your models here.
class Estado(models.Model):
    estado = models.CharField(max_length=20)
    def __str__(self):
        return self.estado

class Perro(models.Model):
    nombre = models.CharField(max_length=50) 
    raza = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=200)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)

    def __str__(self):
        return '{} {} {} {} '.format(self.nombre, self.raza, self.descripcion, self.estado)